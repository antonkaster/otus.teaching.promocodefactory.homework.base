﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleItemRequest
    {
        public Guid Id { get; set; }
    }
}