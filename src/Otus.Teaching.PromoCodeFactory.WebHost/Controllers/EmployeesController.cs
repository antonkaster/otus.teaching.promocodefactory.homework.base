﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Extensions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roles;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roles)
        {
            _employeeRepository = employeeRepository;
            _roles = roles;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            return employee.ToEployeeResponse();
        }

        /// <summary>
        /// Создать сотрудника с указанным id
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <param name="employeeData">Данные сотрудника</param>
        /// <returns></returns>
        [HttpPut("{id:guid}/create")]        
        public async Task<ActionResult<EmployeeResponse>> CreateEmployee(Guid id, [FromBody] EmployeeRequest employeeData)
        {
            if (employeeData == null || employeeData.Email == null
                || employeeData.FirstName == null || employeeData.LastName == null)
                return BadRequest();

            Employee newEmployee = new Employee()
            {
                Id = id,
                FirstName = employeeData.FirstName,
                LastName = employeeData.LastName,
                Email = employeeData.Email,
                AppliedPromocodesCount = 0,
                Roles = employeeData.Roles
                    .Select(r => _roles.GetByIdAsync(r.Id).ConfigureAwait(false).GetAwaiter().GetResult())
                    .ToList()
            };

            await _employeeRepository.Add(newEmployee);

            return newEmployee.ToEployeeResponse();
        }

        /// <summary>
        /// Изменить данные сотрудника с указанным id
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <param name="employeeData">Данные сотрудника</param>
        /// <returns></returns>
        [HttpPatch("{id:guid}/update")]        
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployee(Guid id, [FromBody] EmployeeRequest employeeData)
        {
            if (employeeData == null)
                return BadRequest();

            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            UpdateEmployeeFromEmployeeRequest(employee, employeeData);

            await _employeeRepository.Update(employee);

            return employee.ToEployeeResponse();
        }
        
        /// <summary>
        /// Удалить сотрудника с указанным id
        /// </summary>
        /// <param name="id">Id сотрудника</param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]        
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            await _employeeRepository.Remove(id);

            return Ok();
        }


        private void UpdateEmployeeFromEmployeeRequest(Employee employeeToUpdate, EmployeeRequest employeeRequestData)
        {
            if (employeeToUpdate == null)
                throw new ArgumentNullException("Employee to update can't be null!");
            if (employeeRequestData == null)
                return;

            if (employeeRequestData.Email != null)
                employeeToUpdate.Email = employeeRequestData.Email;
            if (employeeRequestData.FirstName != null)
                employeeToUpdate.FirstName = employeeRequestData.FirstName;
            if (employeeRequestData.LastName != null)
                employeeToUpdate.LastName = employeeRequestData.LastName;
            if (employeeRequestData.Roles != null)
                employeeToUpdate.Roles = employeeRequestData.Roles
                    .Select(r => _roles.GetByIdAsync(r.Id).ConfigureAwait(false).GetAwaiter().GetResult())
                    .ToList();
        }
    }
}