﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected ICollection<T> Data { get; set; }

        public InMemoryRepository(ICollection<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task Add(T itemToAdd)
        {
            return Task.Run(() => Data.Add(itemToAdd));
        }

        public async Task Update(T itemToUpdate)
        {
            await Remove(itemToUpdate.Id);
            await Add(itemToUpdate);
        }

        public async Task Remove(Guid id)
        {            
            T item = await GetByIdAsync(id);
            Data.Remove(item);
        }

    }
}